package co.simplon.promo16;

public class Initialization {
    Board board;

    public Initialization(Board gameBoard) {
        this.board = gameBoard;
    }

    public void board() {
        for (int rowIndex = 0; rowIndex < board.gameBoard[0].length; rowIndex++) {
            for (int colIndex = 0; colIndex < board.gameBoard.length; colIndex++) {
                if (board.gameBoard[rowIndex][colIndex] != null) {
                    System.out.print(board.gameBoard[rowIndex][colIndex].typeOfPawn());
                } else {
                    System.out.print("┌" + "-" + "┐");
                }
            }
            System.out.println();
        }
    }

    public String placingPawnColor(int x, int y, Boolean white, boolean king) {
        Pawn pawn = new Pawn(x, y, white, king);
        board.gameBoard[x][y] = pawn;
        return pawn.typeOfPawn();
    }

    public void boardInstall() {
        // BLACKPAWN
        // Left
        placingPawnColor(3, 0, false, false);
        placingPawnColor(4, 0, false, false);
        placingPawnColor(5, 0, false, false);
        placingPawnColor(4, 1, false, false);
        // Right
        placingPawnColor(3, 8, false, false);
        placingPawnColor(4, 8, false, false);
        placingPawnColor(5, 8, false, false);
        placingPawnColor(4, 7, false, false);
        // Top
        placingPawnColor(0, 3, false, false);
        placingPawnColor(0, 4, false, false);
        placingPawnColor(0, 5, false, false);
        placingPawnColor(1, 4, false, false);
        // Back
        placingPawnColor(8, 3, false, false);
        placingPawnColor(8, 4, false, false);
        placingPawnColor(8, 5, false, false);
        placingPawnColor(7, 4, false, false);

        // WHITE PAWN
        // King
        placingPawnColor(4, 4, true, true);
        // // Left
        placingPawnColor(4, 3, true, false);
        placingPawnColor(4, 2, true, false);
        // Top
        placingPawnColor(3, 4, true, false);
        placingPawnColor(2, 4, true, false);
        // // Right
        placingPawnColor(4, 5, true, false);
        placingPawnColor(4, 6, true, false);
        // // Back
        placingPawnColor(5, 4, true, false);
        placingPawnColor(6, 4, true, false);

        // CORNER BOARD
        placingPawnColor(0, 0, null, false);
        board.gameBoard[0][0].isACorner();
        placingPawnColor(0, 8, null, false);
        board.gameBoard[0][8].isACorner();
        placingPawnColor(8, 0, null, false);
        board.gameBoard[8][0].isACorner();
        placingPawnColor(8, 8, null, false);
        board.gameBoard[8][8].isACorner();
    }
}
