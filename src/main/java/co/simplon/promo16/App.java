package co.simplon.promo16;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        
        Board board = new Board();
        Initialization init = new Initialization(board);
        init.boardInstall();
        board.movePawn(3, 4, 3, 5);
        System.out.println();
        init.board();
    }
}
