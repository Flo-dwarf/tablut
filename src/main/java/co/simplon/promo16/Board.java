package co.simplon.promo16;

public class Board {

    // Initialization init = new Initialization();
    Pawn pawn = new Pawn();

    private final int nbRow = 9;
    private final int nbCol = 9;
    public Pawn[][] gameBoard = new Pawn[nbRow][nbCol];

    public boolean isInBounds(int xPosition, int yPosition) {
        if (xPosition < gameBoard[0].length && xPosition >= 0 &&
                yPosition < gameBoard.length && yPosition >= 0) {
            System.out.println("in bounds");
            return true;
        }
        System.out.println("Hors du plateau !");
        return false;
    }

    public boolean dontMeetPawn(int xPosition, int yPosition) {
        int currX = 3;
        int currY = 4;

        System.out.println("currX " + currX);
        System.out.println("currY " + currY);

        int smallerVal;
        int largerVal;
        if (currX == xPosition) {
            System.out.println("currX == xPosition");
            if (currY > yPosition) {
                smallerVal = yPosition;
                largerVal = currY;
            } else if (yPosition > currY) {
                smallerVal = currY;
                largerVal = yPosition;
            } else
                return false;

            smallerVal++;
            for (; smallerVal < largerVal; smallerVal++) {
                if (gameBoard[smallerVal][currY] != null) {
                    return false;
                }
            }
            return true;
        }

        if (currY == yPosition) {
            System.out.println("currY == yPosition");
            if (currX > xPosition) {
                smallerVal = xPosition;
                largerVal = currX;
            } else if (xPosition > currX) {
                smallerVal = currX;
                largerVal = xPosition;
            } else
                return false;

            smallerVal++;
            for (; smallerVal < largerVal; smallerVal++) {
                if (gameBoard[smallerVal][currY] != null) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public boolean canMoveTo(int xPosition, int yPosition) {
        if (isInBounds(xPosition, yPosition)) {
            return dontMeetPawn(xPosition, yPosition);
        }
        return false;
    }

    public void movePawn(int currentPositionX, int currentPositionY, int movePositionX, int movePositionY) {
        if (gameBoard[movePositionX][movePositionY] == null && canMoveTo(movePositionX, movePositionY)) {
            System.out.println("TEST 5");
            gameBoard[movePositionX][movePositionY] = gameBoard[currentPositionX][currentPositionY];
            gameBoard[currentPositionX][currentPositionY] = null;
        } else {
            System.out.println();
            System.out.println("Déplacement pas possible !");
        }
    }
}
