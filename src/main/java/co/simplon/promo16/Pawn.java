package co.simplon.promo16;

public class Pawn {
    int rowIndexPawn;
    int colIndexPawn;
    Boolean isWhite;
    boolean isPawnOrKing;
    boolean isCorner;

    String side = "│";
    String top = "─";

    String topLeft = "┌";
    String topRight = "┐";
    String baseLeft = "└";
    String baseRight = "┘";

    String lc = "├";
    String rc = "┤";
    String tc = "┬";
    String bc = "┴";
    String crs = "┼";

    public Pawn() {
        this.rowIndexPawn = 0;
        this.colIndexPawn = 0;
    }

    public Pawn(int rowIndexPawn, int colIndexPawn, Boolean isWhite, boolean isPawnOrKing) {
        this.rowIndexPawn = rowIndexPawn;
        this.colIndexPawn = colIndexPawn;
        this.isWhite = isWhite;
        this.isPawnOrKing = isPawnOrKing;
    }

    public int getRowIndexPawn() {
        return rowIndexPawn;
    }

    public void setRowIndexPawn(int rowIndexPawn) {
        this.rowIndexPawn = rowIndexPawn;
    }

    public int getColIndexPawn() {
        return colIndexPawn;
    }

    public void setColIndexPawn(int colIndexPawn) {
        this.colIndexPawn = colIndexPawn;
    }

    public boolean isCorner() {
        return isCorner;
    }

    public void setCorner(boolean isCorner) {
        this.isCorner = isCorner;
    }

    public String typeOfPawn() {
        if (isWhite == null) {
            return boxPawn("corner");
        }
        if (isWhite && isPawnOrKing) {
            return boxPawn("king");
        }
        if (isWhite) {
            return boxPawn("white");
        }
        return boxPawn("black");
    }

    public boolean isACorner() {
        return isCorner = !isCorner;
    }

    public String boxPawn(String pawnType) {
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_BLUE = "\u001B[34m";
        final String BLUE_BOLD = "\033[1;34m";
        final String ANSI_BLACK = "\033[0;30m";
        final String ANSI_RESET = "\u001B[0m";
        String black = ANSI_RED + "X" + ANSI_RESET;
        String white = ANSI_BLUE + "O" + ANSI_RESET;
        String king = BLUE_BOLD + "K" + ANSI_RESET;
        String corner = ANSI_BLACK + "N" + ANSI_RESET;

        switch (pawnType) {
            case "black":
                return topLeft + black + topRight;
            case "white":
                return topLeft + white + topRight;
            case "king":
                return topLeft + king + topRight;
            case "corner":
                return topLeft + corner + topRight;
            default:
                return topLeft + "E" + topRight;
        }
    }
}
